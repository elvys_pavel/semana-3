import { Component, EventEmitter, Output } from "@angular/core";

@Component({
   selector: "SearchForm",
   moduleId: module.id,
   template: `
   <FlexboxLayout flexDirection="row">
   <TextField #texto="ngModel" [(ngModel)]="textFieldValue"
   hint="Ingresar texto..." required minlen="2">
   </TextField>
   <Label *ngIf="texto.hasError('required')" text="*"></Label>
   <Label *ngIf="!texto.hasError('required')
   && texto.hasError('minlen')" text="2+">
   </Label>
   </FlexboxLayout>
   <Button text="Buscar!" (tap)="onButtonTap()" *ngIf="texto.valid"></Button> 
   `
})
export class SearchFormComponent {
   textFieldValue: string = "";
   @Output() search: EventEmitter<string> = new EventEmitter();

   onButtonTap(): void {
      console.log(this.textFieldValue);
      if (this.textFieldValue.length > 1) {
         this.search.emit(this.textFieldValue);
      }
   }
}