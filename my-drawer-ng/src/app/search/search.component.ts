import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { Store } from "@ngrx/store";
import * as SocialShare from "nativescript-social-share";
import { AppState } from "../app.module";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { Application } from "@nativescript/core";
import { NoticiasService } from "../domain/noticias.service";
import { Noticia, NuevaNoticiaAction } from "../domain/noticias-state.model";
import * as Toast from "nativescript-toasts";
import { compose } from "nativescript-email";
const fs = require("file-system"); // usas el filesystem para adjuntar un archivo
const appFolder = fs.knownFolders.currentApp(); // esto te da un objeto de tipo Folder
const appPath = appFolder.path; // esto te da el path a la carpeta src
const logoPath = appPath + "/app/res/icon.png"; // aquí armas el path del archivo copiado  

@Component({
    selector: "Search",
    templateUrl: "./search.component.html",
})
export class SearchComponent implements OnInit {
    // resultados: Array<string> = [];
    resultados: Array<string>;
    @ViewChild("layout") layout: ElementRef;

    constructor(
        public noticias: NoticiasService,
        private store: Store<AppState>
    ) {
        // Use the component constructor to inject providers.
    }
    ngOnInit(): void {
        this.store
            .select((state) => state.noticias.sugerida)
            .subscribe((data) => {
                const f = data;
                if (f != null) {
                    Toast.show({
                        text: "Sugerimos leer: " + f.titulo,
                        duration: Toast.DURATION.SHORT,
                    });
                }
            });
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>Application.getRootView();
        sideDrawer.showDrawer();
    }

    onItemTap(args): void {
        this.store.dispatch(new NuevaNoticiaAction(new Noticia(args.view.bindingContext)));
    }

    onLongPress(s): void {
        console.log(s);
        SocialShare.shareText(s, "Asunto: Compartido desde el curso!");
    }

    buscarAhora(s: string) {
        console.dir("buscarAhora" + s);
        this.noticias.buscar(s).then((r: any) => {
            console.log("resultados buscarAhora: " + JSON.stringify(r));
            this.resultados = r;
        }, (e) => {
            console.log("error buscarAhora: " + e);
            Toast.show({text: "Error en la busqueda", duration: Toast.DURATION.SHORT});
        });
    }
    // const fs = require("file-system"); // usas el filesystem para adjuntar un archivo
    // const appFolder = fs.knownFolders.currentApp(); // esto te da un objeto de tipo Folder
    // const appPath = appFolder.path; // esto te da el path a la carpeta src
    // const logoPath = appPath + "/app/res/icon.png"; // aquí armas el path del archivo copiado  
    
    // compose({
    //     subject: "Mail de Prueba", // asunto del mail
    //     body: "Hola <strong>mundo!</strong> :)", // cuerpo que será enviado
    //     to: ["mail@mail.com"], //lista de destinatarios principales
    //     cc: [], //lista de destinatarios en copia
    //     bcc: [], //lista de destinatarios en copia oculta
    //     attachments: [ //listado de archivos adjuntos
    //      {
    //         fileName: "arrow1.png", // este archivo adjunto está en formato base 64 representado por un string
    //         path:
    // "base64://iVBORw0KGgoAAAANSUhEUgAAABYAAAAoCAYAAAD6xArmAAAACXBIWXMAABYlAAAWJQFJUiTwAAAAHGlET1QAAAACAAAAAAAAABQAAAAoAAAAFAAAABQAAAB5EsHiAAAAAEVJREFUSA1i
    //  YKAimDhxYjwIU9FIBgaQgZMmTfoPwlOmTJGniuHIhlLNxaOGwiNqNEypkwlGk9RokoIUfaM5ijo5Clh9AAAAAP//ksWFvgAAAEFJREFUY5g4cWL8pEmT/        oMwiM1ATTBqONbQHA2W0WDBGgJYBUdTy2iwYA0BrILDI7VMmTJFHqv3yBUEBQsIg/QDAJNpcv6v+k1ZAAAAAElFTkSuQmCC", 
            
    //         mimeType: "image/png" 
    //     },
    //     {
    //         fileName: "icon.png", // este archivo es el que lees directo del filesystem del mobile
    //         path: logoPath,
    //         mimeType: "image/png"
    //     }]
    // }).then(() => console.log("Enviador de mail cerrado"), (err) => console.log("Error: " + err)); 


    // onPull(e) {
    //     console.log(e);
    //     const pullRefresh = e.object;
    //     setTimeout(() => {
    //         this.resultados.push("xxxxxxx");
    //         pullRefresh.refreshing = false;
    //         }, 2000);
    //     }
}
